package com.partnerservice.client;

import com.partnerservice.model.Partners;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "admitadClient", url = "https://api.admitad.com")
public interface AdmitadContentClient {

    @GetMapping(value = "/advcampaigns/?website=${websiteId}&limit=${limit}")
    Partners partnerFromSite();
}