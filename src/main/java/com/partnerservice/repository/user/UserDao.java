package com.partnerservice.repository.user;

import com.partnerservice.model.User;

import java.util.Optional;

public interface UserDao {

    Optional<User> findUserByLogin(String login);
}
