package com.partnerservice.repository.user;

import com.partnerservice.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
@Slf4j
public class UserDaoImpl implements UserDao {

    private final UserRepository userRepository;

    @Override
    public Optional<User> findUserByLogin(String login) {
        log.info("UserDaoImpl findUserByLogin login - {}", login);
        return userRepository.findUserByLogin(login);
    }
}
