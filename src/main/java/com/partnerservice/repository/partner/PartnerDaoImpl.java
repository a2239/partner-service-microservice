package com.partnerservice.repository.partner;

import com.partnerservice.model.Partner;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
@RequiredArgsConstructor
public class PartnerDaoImpl implements PartnerDao {

    private final PartnerRepository partnerRepository;

    @Override
    public List<Partner> findAll() {
        log.info("PartnerDaoImpl findAll");
        return partnerRepository.findAll();
    }

    @Override
    public Partner findById(Long id) {
        log.info("PartnerDaoImpl findById - {}", id);
        return partnerRepository.findById(id)
                .orElseThrow();
    }

    @Override
    public Partner save(Partner partner) {
        log.info("PartnerDaoImpl save partner- {}", partner);
        return partnerRepository.save(partner);
    }

    @Override
    public List<Partner> saveAll(List<Partner> partners) {
        log.info("PartnerDaoImpl saveAll - {}", partners);
        partners = partners
                .stream()
                .peek(partner -> partner.setLastUpdate(LocalDateTime.now()))
                .collect(Collectors.toList());

        return partnerRepository.saveAll(partners);
    }
}