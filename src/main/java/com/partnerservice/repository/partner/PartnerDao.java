package com.partnerservice.repository.partner;

import com.partnerservice.model.Partner;

import java.util.List;

public interface PartnerDao {

    List<Partner> findAll();

    Partner findById(Long id);

    Partner save(Partner partner);

    List<Partner> saveAll(List<Partner> partners);

}
