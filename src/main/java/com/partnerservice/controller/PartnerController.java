package com.partnerservice.controller;

import com.partnerservice.model.Partner;
import com.partnerservice.model.Partners;
import com.partnerservice.repository.partner.PartnerDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("partners")
@RequiredArgsConstructor
public class PartnerController {

    private final PartnerDao partnerDao;

    @GetMapping
    public Partners findAll() {
        log.info("PartnerController findAll");
        Partners partners = new Partners();
        partners.setPartners(partnerDao.findAll());
        return partners;
    }

    @GetMapping("/{id}")
    public Partner findById(@PathVariable Long id) {
        log.info("PartnerController findById - {}", id);
        return partnerDao.findById(id);
    }

}
