package com.partnerservice.model;

public enum Status {
    ACTIVE, BANNED
}
