package com.partnerservice.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Permission {

    PARTNER_READ("partner:read"),
    PARTNER_WRITE("partner:write");

    private final String permission;

}
