package com.partnerservice.model;

import lombok.Data;

@Data
public class ErrorResponse {

    private String message;

}
