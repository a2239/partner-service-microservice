package com.partnerservice.producer;

import com.partnerservice.client.AdmitadContentClient;
import com.partnerservice.model.Partners;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PartnerProducer {

    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final AdmitadContentClient admitadContentClient;

    @Scheduled(cron = "0 59 23 * * MON-SUN")
    public void partnerProduce() {
        log.info("PartnerProducer partnerProduce begin send message");
        Partners partners = admitadContentClient.partnerFromSite();
        kafkaTemplate.send("partners-topic", partners);
        log.info("PartnerProducer partnerProduce, message have sent partners - {}", partners);
    }
}
